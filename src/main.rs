mod adapters;
mod data;

use actix_web::{web, middleware, App, HttpServer};


#[actix_web::main]
async fn main() -> std::io::Result<()> {
     // Start N db executor actors (N = number of cores avail)
    let database = adapters::providers::database::DatabaseAccess::create();

    let factory = move || {
        let api_scope = adapters::portals::commend_controller::configure(web::scope("/api"));
        let html_scope = adapters::portals::commend_page::configure(web::scope(""));
        App::new()
            .data(database.clone())
            .wrap(middleware::Logger::default())
            .service(api_scope)
            .service(html_scope)
    };
    HttpServer::new(factory)
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}
