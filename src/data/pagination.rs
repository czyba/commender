use serde::Deserialize;
use std::num::NonZeroU32;

#[derive(Deserialize)]
pub struct Page {
    pub size: Option<NonZeroU32>,
    pub page: Option<u32>,
}

impl Page {
    pub fn get_size(&self) -> u32 {
        self.size.map(u32::from).unwrap_or(10)
    }

    pub fn get_page(&self) -> u32 {
        self.page.unwrap_or(0)
    }

    pub fn get_offset(&self) -> u32 {
        //TODO: Not safe
        self.get_page() * self.get_size()
    }
}
