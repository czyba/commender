use serde::{Deserialize, Serialize};
use chrono::{DateTime, Utc};

#[derive(Serialize)]
pub struct JsonCommend {
    from: String,
    message: String,
    time: DateTime<Utc>,
}

#[derive(Deserialize)]
pub struct FormDataCommend {
    to: String,
    message: String,
    public: bool,
}

pub struct DatabaseCommend {
    pub from: String,
    pub to: String,
    pub message: String,
    pub public: bool,
    pub time: DateTime<Utc>,
}

impl DatabaseCommend {
    pub fn new(
        from: String,
        to: String,
        message: String,
        public: bool,
        time: DateTime<Utc>
    ) -> DatabaseCommend {
        DatabaseCommend {
            from: from.to_owned(),
            to: to.to_owned(),
            message: message.to_owned(),
            public,
            time
        }
    }
}

impl From<DatabaseCommend> for JsonCommend {
    fn from(commend: DatabaseCommend) -> Self {
        JsonCommend {
            from: commend.from,
            message: commend.message,
            time: commend.time,
        }
    }
}

impl From<(FormDataCommend, &str)> for DatabaseCommend {
    fn from(data_from_pair: (FormDataCommend, &str)) -> Self { 
        DatabaseCommend {
            from: data_from_pair.1.to_owned(),
            to: data_from_pair.0.to,
            message: data_from_pair.0.message,
            public: data_from_pair.0.public,
            time: Utc::now()
        }
    }
}
