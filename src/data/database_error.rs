#[derive(Debug)]
pub struct DatabaseError;

impl std::fmt::Display for DatabaseError {
    
    fn fmt(
        &self,
        fmt: &mut std::fmt::Formatter<'_>
    ) -> std::result::Result<(), std::fmt::Error> { 
        fmt.write_str("A database error has occurred.")
    }
}
