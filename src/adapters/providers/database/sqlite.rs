use r2d2_sqlite::{self, SqliteConnectionManager};
use rusqlite::{params, Statement};

use crate::data::{DatabaseCommend, DatabaseError, Page};

pub type Pool = r2d2::Pool<r2d2_sqlite::SqliteConnectionManager>;

#[derive(Clone)]
pub struct DatabaseAccess {
    pool: Pool,
}

impl DatabaseAccess {
    pub fn create() -> DatabaseAccess {
        let manager = SqliteConnectionManager::file("commends.db");
        // Ok to crash here. Otherwise we have no db anyways
        let pool = Pool::new(manager).unwrap();
        DatabaseAccess {
            pool
        }
    }

    pub fn get_newest_public_commends(&self, page: &Page) -> Result<Vec<DatabaseCommend>, DatabaseError> {
        let conn = self.pool.get()?;
        let stmt = conn.prepare(
            "
            SELECT 
                SENDER,
                RECEIVER,
                MESSAGE,
                PUBLIC,
                TIME
            FROM 
                commends
            WHERE
                PUBLIC = 1
            ORDER BY
                TIME DESC
            LIMIT
                ?1
            OFFSET
                ?2",
        )?;
        Ok(get_commends_from_statement(stmt, page)?)
    }

    pub fn store_commend(&self, commend: DatabaseCommend) -> Result<usize, DatabaseError> {
        let conn = self.pool.get()?;
        let mut stmt = conn.prepare(
            "
            INSERT INTO commends (
                SENDER,
                RECEIVER,
                MESSAGE,
                PUBLIC,
                TIME
            ) VALUES (
                ?1,
                ?2,
                ?3,
                ?4,
                ?5
            )",
        )?;
        Ok(stmt.execute(params![
            commend.from,
            commend.to,
            commend.message,
            commend.public,
            commend.time
        ])?)
    }
}

fn get_commends_from_statement(
    mut statement: Statement, 
    page: &Page
) -> Result<Vec<DatabaseCommend>, rusqlite::Error> {
    statement
        .query_map(
            params![page.get_size(), page.get_offset()],
            |row| {
            Ok(DatabaseCommend::new(
                row.get(0)?,
                row.get(1)?,
                row.get(2)?,
                row.get(3)?,
                row.get(4)?,
            ))
        })
        .and_then(Iterator::collect)
}

impl From<rusqlite::Error> for DatabaseError {
    
    fn from(_: rusqlite::Error) -> Self { 
        DatabaseError 
    }
}
