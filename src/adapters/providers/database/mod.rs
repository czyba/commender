mod sqlite;

pub type DatabaseAccess = sqlite::DatabaseAccess;

impl From<r2d2::Error> for crate::data::DatabaseError {
    
    fn from(_: r2d2::Error) -> Self { 
        crate::data::DatabaseError 
    }
}
