use actix_web::{get, post, web, Scope};

use crate::adapters::providers::database::DatabaseAccess;
use crate::data::{DatabaseCommend, DatabaseError, Page, FormDataCommend, JsonCommend};


pub fn configure(scope: Scope) -> Scope {
    scope
        .service(get_commends)
        .service(post_commend)
}

#[get("/commends")]
async fn get_commends(
    db: web::Data<DatabaseAccess>,
    page: web::Query<Page>
) -> actix_web::Result<web::Json<Vec<JsonCommend>>> {
    let commends = db.get_newest_public_commends(&page)?;
    let commends: Vec<JsonCommend> = commends.into_iter().map(JsonCommend::from).collect();
    Ok(web::Json(
        commends
    ))
}

#[post("/commends")]
async fn post_commend(
    data: web::Json<FormDataCommend>,
    db: web::Data<DatabaseAccess>
) -> actix_web::Result<&'static str> {
    let db_commend = DatabaseCommend::from((data.into_inner(), "ME"));
    db.store_commend(db_commend)?;
    Ok("")
}

impl actix_web::ResponseError for DatabaseError {

}
