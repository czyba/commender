use actix_web::{get, web, Responder, Scope};
use maud::html;

use crate::adapters::providers::database::DatabaseAccess;
use crate::data::{DatabaseCommend, Page};

pub fn configure(scope: Scope) -> Scope {
    scope
        .service(index)
}

#[get("/index.html")]
async fn index (
    db: web::Data<DatabaseAccess>,
    page: web::Query<Page>
) -> impl Responder {
    db.get_newest_public_commends(&page)
        .map(list_commends).unwrap()
}


fn list_commends(commends: Vec<DatabaseCommend>) -> maud::Markup {
    html! {
        div {
            @for commend in &commends {
                (commend_to_html(&commend))
            }
        }
    }
}


fn commend_to_html(commend: &DatabaseCommend) -> maud::Markup {
    html! {
        h1 {
            "From " (commend.from) " at " (commend.time)
        }
        p {
            (commend.message)
        }
    }
}